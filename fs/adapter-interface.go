package fs

type Adapter interface {
	CreateRepo(username string, repoName string) error
	ListRepos(username string) ([]string, error)
	ListRepoFiles(username string, repo string) ([]string, error)
}
