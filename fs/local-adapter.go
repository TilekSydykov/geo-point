package fs

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const (
	dirname         = "./repos"
	configFolder    = ".papuri"
	mainFile        = "main.papuri"
	configBegin     = "---papuri-config---"
	resourcesFolder = "resources"
	gitKeep         = ".gitkeep"
)

type LocalAdapter struct {
	Adapter
}

func (a *LocalAdapter) CreateRepo(username string, repoName string) error {
	var dirName = getRepoFilepath(username, repoName)
	if _, err := os.Stat(dirName); !os.IsNotExist(err) {
		return errors.New("repo exists")
	}
	err := os.MkdirAll(filepath.Join(dirName, configFolder), os.ModePerm)
	err = os.MkdirAll(filepath.Join(dirName, resourcesFolder), os.ModePerm)
	cmd := exec.Command("git", "init")
	cmd.Dir = dirName
	_, err = cmd.Output()
	if err != nil {
		return errors.New("init error")
	}
	err = writeFile(filepath.Join(dirName, configFolder, mainFile), configBegin)
	err = writeFile(filepath.Join(dirName, resourcesFolder, gitKeep), "")
	return err
}

func (a *LocalAdapter) ListRepos(username string) ([]string, error) {
	var dirName = filepath.Join(dirname, strings.ToLower(username))
	return filePathWalkDir(dirName)
}

func (a *LocalAdapter) ListRepoFiles(username string, repo string) ([]string, error) {
	var dirName = filepath.Join(dirname, strings.ToLower(username), strings.ToLower(repo))
	return filePathWalkDir(dirName)
}

func filePathWalkDir(root string) ([]string, error) {
	var files []string
	length := len(root) + 1
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path[length:])
		}
		return nil
	})
	return files, err
}

func writeFile(path string, data string) error {
	file, err := os.Create(path)
	if err != nil {
		_ = file.Close()
		return err
	}
	_, err2 := file.WriteString(data)
	_ = file.Close()
	if err2 != nil {
		return err
	}
	return nil
}

func getRepoFilepath(username string, repoName string) string {
	return filepath.Join(
		dirname,
		strings.ToLower(username),
		strings.ToLower(repoName),
	)
}
