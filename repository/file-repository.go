package repository

import (
	"gorm.io/gorm"
	"papuri/entity"
	"papuri/repository/database"
)

type FileRepository interface {
	Create(file *entity.File)
	Update(file *entity.File)
	Delete(file *entity.File)
	All() []*entity.File
	GetFileByNameAndRepoId(username string, id uint64) *entity.File
	GetFileById(id string) *entity.File
	UpdateFiles(files []*entity.File, repoId uint64) error
	ListFilesByRepo(id uint64) *[]entity.File
}

type fileRepository struct {
	connection *gorm.DB
}

func NewFileRepository() FileRepository {
	return &fileRepository{
		connection: database.DB,
	}
}

func (db *fileRepository) UpdateFiles(files []*entity.File, repoId uint64) error {
	tx := db.connection.Begin()
	for _, file := range files {
		if len(file.Path) > 0 {
			tx.Model(entity.File{}).Where("repo_id = ? AND id = ?", repoId, file.Id).Update("path", file.Path)
		}
		if len(file.Name) > 0 {
			tx.Model(entity.File{}).Where("repo_id = ? AND id = ?", repoId, file.Id).Update("name", file.Name)
		}
		if len(file.Content) > 0 {
			tx.Model(entity.File{}).Where("repo_id = ? AND id = ?", repoId, file.Id).Update("content", file.Content)
		}
	}
	tx.Commit()
	return nil
}

func (db *fileRepository) ListFilesByRepo(id uint64) *[]entity.File {
	files := &[]entity.File{}
	db.connection.Where("repo_id = ?", id).Find(&files)
	return files
}

func (db *fileRepository) GetFileByNameAndRepoId(path string, id uint64) *entity.File {
	file := &entity.File{}
	db.connection.Where("path = ? AND repo_id = ?", path, id).First(&file)
	if file.Id == "" {
		return nil
	}
	return file
}

func (db *fileRepository) GetFileById(id string) *entity.File {
	file := &entity.File{}
	db.connection.Where("uid = ?", id).First(&file)
	return file
}

func (db *fileRepository) All() []*entity.File {
	var users []*entity.File
	db.connection.Find(&users)
	return users
}

func (db *fileRepository) Delete(file *entity.File) {
	db.connection.Delete(file)
}

func (db *fileRepository) Update(file *entity.File) {
	db.connection.Save(&file)
}

func (db *fileRepository) Create(file *entity.File) {
	db.connection.Save(file)
}
