package repository

import (
	"gorm.io/gorm"
	"papuri/entity"
	"papuri/repository/database"
)

type MediaRepository interface {
	Create(media *entity.Media)
	Update(media *entity.Media)
	Delete(media *entity.Media)
	All() []*entity.Media
	GetMediaById(id uint64) *entity.Media
}

type mediaRepository struct {
	connection *gorm.DB
}

func NewMediaRepository() MediaRepository {
	return &mediaRepository{
		connection: database.DB,
	}
}

func (db *mediaRepository) GetMediaById(id uint64) *entity.Media {
	media := &entity.Media{}
	db.connection.Where("id = ?", id).First(&media)
	return media
}

func (db *mediaRepository) All() []*entity.Media {
	var medias []*entity.Media
	db.connection.Find(&medias)
	return medias
}

func (db *mediaRepository) Delete(media *entity.Media) {
	db.connection.Delete(media)
}

func (db *mediaRepository) Update(media *entity.Media) {
	db.connection.Save(&media)
}

func (db *mediaRepository) Create(media *entity.Media) {
	db.connection.Save(media)
}
