package repository

import (
	"gorm.io/gorm"
	"papuri/entity"
	"papuri/repository/database"
)

type RepoRepository interface {
	Create(repo *entity.Repo)
	Update(repo *entity.Repo)
	Delete(repo *entity.Repo)
	All() []*entity.Repo
	GetRepoByName(name string) *entity.Repo
	GetRepoByNameAndUserName(name string, username string) *entity.Repo
	GetRepoByUserName(name string) *[]entity.Repo
	GetRepoById(id uint64) *entity.Repo
	GetByUserId(id uint64) []*entity.Repo
}

type repoRepository struct {
	connection *gorm.DB
}

func (db *repoRepository) GetByUserId(id uint64) []*entity.Repo {
	var repos []*entity.Repo
	db.connection.Where("user_id = ?", id).Find(&repos)
	return repos
}

func NewRepoRepository() RepoRepository {
	return &repoRepository{
		connection: database.DB,
	}
}

func (db *repoRepository) GetRepoByName(reponame string) *entity.Repo {
	repo := &entity.Repo{}
	db.connection.Where("name = ?", reponame).First(&repo)
	if repo.Id == 0 {
		return nil
	}
	return repo
}

func (db *repoRepository) GetRepoByNameAndUserName(name string, username string) *entity.Repo {
	repo := &entity.Repo{}
	db.connection.Where("owner_user_name = ? AND name = ?", username, name).First(&repo)
	if repo.Id == 0 {
		return nil
	}
	return repo
}

func (db *repoRepository) GetRepoByUserName(name string) *[]entity.Repo {
	repos := &[]entity.Repo{}
	db.connection.Where("owner_user_name = ?", name).Find(&repos)

	return repos
}

func (db *repoRepository) GetRepoById(id uint64) *entity.Repo {
	repo := &entity.Repo{}
	db.connection.Where("id = ?", id).First(&repo)
	return repo
}

func (db *repoRepository) GetRepoByIdAndCountValidation(id uint64) *entity.Repo {
	repo := &entity.Repo{}
	db.connection.Where("id = ?", id).First(&repo)
	return repo
}

func (db *repoRepository) All() []*entity.Repo {
	var repos []*entity.Repo
	db.connection.Find(&repos)
	return repos
}

func (db *repoRepository) Delete(repo *entity.Repo) {
	db.connection.Delete(repo)
}

func (db *repoRepository) Update(repo *entity.Repo) {
	db.connection.Save(&repo)
}

func (db *repoRepository) Create(repo *entity.Repo) {
	db.connection.Save(repo)
}
