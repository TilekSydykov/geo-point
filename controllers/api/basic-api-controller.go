package api

import (
	"github.com/gin-gonic/gin"
	"papuri/entity"
	"strconv"
)

type BasicApiController struct {
	data map[string]interface{}
	user *entity.User
}

func (b *BasicApiController) Init() {
	b.InitData()
}

func (b *BasicApiController) InitData() {
	b.data = make(map[string]interface{})
	b.user = nil
}

func (b *BasicApiController) ParseIntParams(c *gin.Context, paramNames ...string) ([]uint64, error) {
	var params = make([]uint64, len(paramNames))
	for i, paramName := range paramNames {
		id, err := strconv.ParseUint(c.Param(paramName), 10, 64)
		if err != nil {
			return nil, err
		}
		params[i] = id
	}
	return params, nil
}

func (b *BasicApiController) FetchUser(c *gin.Context) *entity.User {
	b.InitData()
	user, ok := c.Get("user")
	if ok {
		b.user = user.(*entity.User)
	}
	return b.user
}

func (b *BasicApiController) IsLoggedIn() bool {
	return b.user != nil && b.user.Id > 0
}

func (b *BasicApiController) HasLoggedInData(c *gin.Context) bool {
	id, ok := c.Get("userId")
	return ok && id.(uint64) > 0
}

func (b *BasicApiController) ShouldBeLoggedIn(c *gin.Context) {
	if !b.HasLoggedInData(c) {
		c.JSON(403, gin.H{
			"error": "unauthorized",
		})
	}
}

func (b *BasicApiController) Error(c *gin.Context) {
	if !b.HasLoggedInData(c) {
		c.JSON(500, gin.H{
			"error": "internal server error",
		})
	}
}

func (b *BasicApiController) Error404(c *gin.Context) {
	c.JSON(404, gin.H{
		"error": "not found",
	})
}

func (b *BasicApiController) Error400(c *gin.Context, message string) {
	c.JSON(400, gin.H{
		"error": message,
	})
}

func (b *BasicApiController) Render(c *gin.Context, code int) {
	c.JSON(code, b.data)
}

func (b *BasicApiController) AddData(key string, data interface{}) {
	b.data[key] = data
}
