package api

import (
	"github.com/gorilla/websocket"
	"net/http"
	"papuri/entity"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Message struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type Connection struct {
	Conn       *websocket.Conn
	Uuid       string
	Repo       *entity.Repo
	Controller *RepoApiController
}

var connections map[string]Connection

func init() {
	connections = make(map[string]Connection)
}

func messageResolver(m Message, c *Connection) {
	switch m.Type {
	case "test":
		err := c.Conn.WriteJSON(Message{Type: "test", Data: "ok"})
		if err != nil {
			return
		}

	}
}

func (c *Connection) writeRepo() {
	err := c.Conn.WriteJSON(Message{Type: "repo", Data: c.Repo})
	if err != nil {
		return
	}
}
