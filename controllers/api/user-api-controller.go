package api

import (
	"github.com/gin-gonic/gin"
)

type UserApiController struct {
	BasicApiController
}

func (m *UserApiController) GetMe(c *gin.Context) {
	m.FetchUser(c)
	c.JSON(200, m.user)
}
