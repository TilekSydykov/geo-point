package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"papuri/entity"
	"papuri/service"
)

type LoginData struct {
	Username string `json:"user_name" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func Register(c *gin.Context) {
	user := &entity.User{}
	obj, ok := service.DataBind(c, user)
	if !ok {
		c.JSON(http.StatusBadRequest, obj.(gin.H))
		return
	}
	parsedUser := obj.(*entity.User)
	err := service.RegisterUser(parsedUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "user registered successfully"})
}

func Login(c *gin.Context) {
	login := &LoginData{}
	obj, ok := service.DataBind(c, login)
	if !ok {
		c.JSON(http.StatusBadRequest, obj.(gin.H))
		return
	}
	parsedLogin := obj.(*LoginData)
	tokens, err := service.SingIn(parsedLogin.Username, parsedLogin.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, tokens)
}

func Refresh(c *gin.Context) {
	tokens := &service.AuthTokens{}
	obj, ok := service.DataBind(c, tokens)
	if !ok {
		c.JSON(http.StatusBadRequest, obj.(gin.H))
		return
	}
	parsedTokens := obj.(*service.AuthTokens)
	err := service.RefreshAccessToken(parsedTokens)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, tokens)
}
