package api

import (
	"github.com/gin-gonic/gin"
	"github.com/kr/pretty"
	"papuri/repository"
	"papuri/utils"
)

type MediaApiController struct {
	BasicApiController
}

func (m *MediaApiController) Create(c *gin.Context) {
	m.FetchUser(c)
	pretty.Println(c.Request.Header.Get("Content-Type"))
	var accessPath = "local:" + utils.UploadHandler(c, "file")
	m.data["message"] = "created"
	m.data["result"] = accessPath
	m.Render(c, 200)
}

func (m *MediaApiController) List(c *gin.Context) {
	m.FetchUser(c)
	m.data["result"] = repository.NewMediaRepository().All()
	m.Render(c, 200)
}

func (m *MediaApiController) GetOne(c *gin.Context) {
	m.FetchUser(c)
	m.Render(c, 200)
}
