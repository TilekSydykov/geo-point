package entity

type Repo struct {
	HasId

	Name          string `json:"name" gorm:"index:repo_uid, unique;type:varchar(1024)" bind:"required"`
	OwnerUserName string `json:"user_name" gorm:"index:repo_uid, unique;type:varchar(1024)"`
	Owner         *User  `json:"user" gorm:"foreignKey:OwnerUserName;references:UserName"`
	Model
}

func (Repo) TableName() string {
	return "repos"
}
