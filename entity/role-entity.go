package entity

type Role struct {
	HasId
	Title      string `json:"title" gorm:"type:varchar(1024)" form:"title"`
	Permission uint64 `json:"permission"`

	Model
}

func (Role) TableName() string {
	return "roles"
}
