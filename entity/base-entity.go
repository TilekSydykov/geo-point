package entity

import (
	"gorm.io/gorm"
	"time"
)

type Model struct {
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

type HasOwner struct {
	UserId uint64 `json:"user_id"`
	User   *User  `json:"user" gorm:"foreignKey:UserId;references:Id"`
}

type HasId struct {
	Id             uint64 `json:"id" gorm:"primaryKey;autoIncrement" form:"-"`
}