package entity

type Media struct {
	HasId
	Name       string `json:"name" gorm:"type:varchar(1024)" bind:"required"`
	Size       uint64 `json:"size"`
	Extension  string `json:"extension"`
	AccessPath string `json:"access_path"`
	OwnerId    uint64 `json:"user_id"`
	Owner      *User  `json:"user" gorm:"foreignKey:OwnerId;references:Id"`
	Model
}

func (Media) TableName() string {
	return "media"
}
