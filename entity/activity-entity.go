package entity

type Activity struct {
	HasId

	Title string `json:"title" gorm:"type:varchar(1024)" form:"title"`

	HasOwner
	Model
}

func (Activity) TableName() string {
	return "activities"
}
