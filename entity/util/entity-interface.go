package util

import "papuri/entity"

var Entities = func() []interface{} {
	ent := func(ent ...interface{}) []interface{} { return ent }
	return ent(
		&entity.Activity{},
		&entity.Notification{},
		&entity.User{},
		&entity.Role{},
		&entity.Repo{},
		&entity.File{},
	)
}
