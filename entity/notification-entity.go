package entity

type Notification struct {
	HasId
	Title      string `json:"title" gorm:"type:varchar(1024)" form:"title"`
	ActivityId uint64 `json:"activity_id"`
	IsRead     bool   `json:"is_read"`

	Activity *Activity `json:"activity" gorm:"foreignKey:ActivityId;references:Id"`
	HasOwner
	Model
}

func (Notification) TableName() string {
	return "notifications"
}
