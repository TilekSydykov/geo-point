CREATE VIEW organisations_view AS
    SELECT
           u.name as name,
           u.username as uid,
           o.description as description,
           u.configs as configs,
           o.configs as organization_configs
    FROM users as u
    JOIN organizations o on u.organisation_id = o.id;

CREATE VIEW users_view AS
    SELECT
           u.name as name,
           u.username as uid,
           u.email as email,
           u.surname,
           u.configs
    FROM users as u
    WHERE u.type = 'user';


