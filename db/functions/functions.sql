CREATE OR REPLACE FUNCTION check_email(email text) RETURNS BOOLEAN AS $$
    BEGIN
        RETURN $1 ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$';
    END
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_bounds(string text, min integer, max integer) RETURNS BOOLEAN AS $$
    BEGIN
        RETURN LENGTH($1) > $2 AND LENGTH($1) < $3;
    END
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION validate_uid(string text) RETURNS BOOLEAN AS $$
BEGIN
    RETURN $1 ~ '^[a-z0-9_]+$' ;
END
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION validate_user_type(user_id bigint, user_type text) RETURNS BOOLEAN AS $$
BEGIN
    RETURN (SELECT "type" FROM "users" WHERE id = $1) = $2::user_type ;
END
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_action(user_id bigint, action_type text) RETURNS bool AS $$
BEGIN
    INSERT INTO activity (user_id, type) VALUES ($1, $2);
    RETURN true;
END
$$
    LANGUAGE plpgsql;