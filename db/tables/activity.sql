CREATE TYPE ACTION_TYPE AS enum ('user_create');

CREATE TABLE activity (
    "id" SERIAL PRIMARY KEY,
    "user_id" BIGINT REFERENCES users(id),
    "type" ACTION_TYPE,
    "data" JSONB,
    "date" timestamp DEFAULT now()
);

