CREATE TYPE USER_TYPE AS ENUM ('user', 'organization');

DROP TABLE "users";

CREATE TABLE "users" (
    "id" SERIAL PRIMARY KEY,
    "name" TEXT,
    "surname" TEXT,
    "email" TEXT,
    "username" TEXT UNIQUE NOT NULL,
    "password" TEXT NOT NULL,
    "type" USER_TYPE DEFAULT 'user',
    "organisation_id" BIGINT
        REFERENCES organizations(id) ON DELETE CASCADE,
    "configs" JSONB,
    CONSTRAINT username_length CHECK ( check_bounds( username, 4, 20 ) ),
    CONSTRAINT email_validation CHECK ( check_email(email) ),
    CONSTRAINT username_validation CHECK ( validate_uid(username) )
);

CREATE INDEX user_usernames ON users(username);

INSERT INTO "users" (name, email, username, password, type)
VALUES ('admin', 'tilek@gmail.com', 'hello', '', 'organization');

SELECT * FROM users;
