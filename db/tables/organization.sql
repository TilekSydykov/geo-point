DROP TABLE "organizations";
DROP TABLE "employees";

CREATE TABLE "organizations" (
    "id" SERIAL PRIMARY KEY,
    "name" TEXT,
    "description" TEXT,
    "configs" JSONB,
    CONSTRAINT name_bounds check ( check_bounds(name, 4, 20) )
);

CREATE TABLE "employees" (
    "user_id" BIGINT REFERENCES "users"(id) ON DELETE CASCADE ON UPDATE CASCADE,
    "organization_id" BIGINT REFERENCES "users"(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CHECK ( validate_user_type(user_id, 'user') ),
    CHECK ( validate_user_type(organization_id, 'organization')),
    UNIQUE (user_id, organization_id)
);

INSERT INTO "employees" (user_id, organization_id) VALUES (2,3);