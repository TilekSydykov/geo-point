CREATE TABLE "groups" (
    "id" SERIAL PRIMARY KEY,
    "name" TEXT,
    "uid" TEXT,
    "configs" JSONB,
    CONSTRAINT username_validation CHECK ( validate_uid(uid) ),
    CONSTRAINT name_bounds check ( check_bounds(name, 4, 20) )
);

CREATE TABLE "group_participant" (
    "user_id" BIGINT REFERENCES "users"(id),
    "group_id" BIGINT REFERENCES "groups"(id)
)