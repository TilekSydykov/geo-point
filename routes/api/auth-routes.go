package routes

import (
	"github.com/gin-gonic/gin"
	"papuri/controllers/api"
)

func AuthRouters(rg *gin.RouterGroup) {
	group := rg.Group("/auth")
	{
		group.POST("/register", api.Register)
		group.POST("/login", api.Login)
		group.POST("/refresh", api.Refresh)
	}
}
