package routes

import (
	"github.com/gin-gonic/gin"
	"papuri/controllers/api"
	"papuri/routes/midlewares"
)

func RepoRoutes(rg *gin.RouterGroup) {
	group := rg.Group("/repos")
	group.Use(midlewares.AuthMiddleware())
	controller := api.RepoApiController{}

	group.POST("", controller.Create)
	group.GET("", controller.ListRepos)
	group.GET("u/:username/:repo", controller.GetRepo)
	group.GET("files/:username/:repo", controller.FetchFiles)
	group.PATCH("files/:username/:repo", controller.UpdateFiles)
	group.POST("files/:username/:repo", controller.AddFile)

	rg.GET("/repos/w/:username/:repo", controller.WebSocketHandler)
}
