package routes

import (
	"github.com/gin-gonic/gin"
	"papuri/controllers/api"
	"papuri/routes/midlewares"
)

func UserRoutes(rg *gin.RouterGroup) {
	group := rg.Group("/user")
	group.Use(midlewares.AuthMiddleware())
	controller := api.UserApiController{}
	{
		group.GET("/me", controller.GetMe)
	}
}
