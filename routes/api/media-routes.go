package routes

import (
	"github.com/gin-gonic/gin"
	"papuri/controllers/api"
	"papuri/routes/midlewares"
)

func MediaRoutes(rg *gin.RouterGroup) {
	group := rg.Group("media")
	group.Use(midlewares.AuthMiddleware())
	controller := api.MediaApiController{}
	{
		group.POST("", controller.Create)
		group.PUT("", controller.Create)
		group.GET("", controller.List)
	}
}
