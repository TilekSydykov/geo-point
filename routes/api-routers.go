package routes

import (
	"github.com/gin-gonic/gin"
	"papuri/routes/api"
)

func ApiRouters(rg *gin.RouterGroup) {
	routes.AuthRouters(rg)
	v1 := rg.Group("v1")
	routes.UserRoutes(v1)
	routes.RepoRoutes(v1)
	routes.MediaRoutes(v1)
}
